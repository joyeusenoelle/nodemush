## NodeMUSH

An implementation of the [MUSH](https://en.wikipedia.org/wiki/MUSH) family of chat servers written in Javascript and built on Node.js.
