const net = require('net'),
      fs = require('fs'),
      db = require('./db');
const args = process.argv.slice(2);

var pd = ((data, c) => {
  console.log(data)
  return data;
});

var cnct = ((data,c) => {
  console.log('connecting...');
  if (c["loggedin"] === true) {
    return "You are already logged in.";
  }
  var chunks = data.split(' ');
  if (chunks.length == 3) {
    console.log(chunks[1] + ' connected with password ' + chunks[2]);
    c["loggedin"] = true;
    return "Logged " + chunks[1] + " in.";
  } else if (chunks.length > 3) {
    console.log(chunks[1] + ' used a space.');
    return "Usernames and passwords cannot contain spaces.";
  } else {
    console.log(chunks[1] + ' did not supply a password.');
    return "You must supply a password.";
  }
});

var crt = ((data, c) => {
  console.log('creating...');
  var chunks = data.split(' ');
  if (c["loggedin"] === true) {
    return "You are already logged in.";
  }
  if (chunks.length === 3) {
    console.log(chunks[1] + ' connected with password ' + chunks[2]);
    c["loggedin"] = true;
    return "Logged " + chunks[1] + " in.";
  } else if (chunks.length > 3) {
    console.log(chunks[1] + ' used a space.');
    return "Usernames and passwords cannot contain spaces.";
  } else {
    console.log(chunks[1] + ' did not supply a password.');
    return "You must supply a password.";
  }
});

const srv = net.createServer((c) => {
  c["loggedin"] = false;
  c["dbref"] = null;
  c.writeln = ((data) => {
    c.write(data + "\n");
  });
  c.setEncoding('utf8');
  console.log('client connected');
  c.write(fs.readFileSync('./default/intro.txt', 'utf8'));
  c.on('data', (d) => {
    if(d.substr(0,7) === "connect") {
      c.writeln(cnct(d,c));
    } else if (d.substr(0,6) === "create") {
      c.writeln(crt(d,c));
    } else {
      c.writeln(pd(d, c));
    }
  })
  c.on('end', () => {
    console.log('client disconnected');
    c.end();
  });
});

srv.listen('8192', () => {
  console.log('connection bound');
});
