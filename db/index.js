const { Pool } = require('pg'),
      format = require('pg-format');
const pool = new Pool();

module.exports = {
  query: (text, params, callback) => {
    return pool.query(text, params, callback);
  }
}
